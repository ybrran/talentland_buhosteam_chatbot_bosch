#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""":

@author: gibbygarcia
"""
from Utils.chatBotUtils import *
from Persistence.Data.jsonData import *
from Persistence.Data.templatesData import *
from Persistence.DBModel.unitisStoreModel import *
from Persistence.DBModel.APIModel import *
from Processing.OCRProcessing import proccessOCRImage_OcrProcessing
import json
#constTicket="#"

def dispatcher_Service(req, contexto):
    intent = getIntentName_FromDlgFlwRequest_Util(req)
    
    switcher = { 
        'Thanks':simpleResponse_Service,
        'Name':simpleResponse_Service,
        
        'MyName':responseToUserName_Service,
        'SparkPlug':responseToSparkPlug_Service,
        'AddToCar': addToCart_Service,
        'ShowCart': getCart_Service,
        'GetCategories': getAllCategores
        
    }
    func = switcher.get(intent, None)
    if func != None:
        return func(req, contexto)
    
def simpleResponse_Service(req, contexto):
    intent = getIntentName_FromDlgFlwRequest_Util(req)
    return getResponseToSendFromJSON_Util(intentsResponses_JsonData(), intent) 
    
def getAllCategores(req, contexto):
    print('llego a categorias') 
    categories = getCategories_APIModel()
    response = ''
    for categorie in categories:
        response = response + categorie.get('categoryName')+'\n'
        
    return generateResponseTemplate_Util(response)
     
    
def responseToUserName_Service(req, contexto): 
    idSender = getSenderIdFromDlgFlwRequest_Util(req)  
    username = getParamFromDlgFlwRequest_Util(req).get('given-name') 
    print('usuario: '+username)
    usuario = findUser_Model(idSender) 
    if usuario == None:  
        usuario = insertUser_Model(idSender, username, None) 
    
    response = generateResponseTemplate_Util('Hi '+username+'')
    return response

def responseToSparkPlug_Service(req, contexto): 
    #required
    year = searchParamForYear(req)
    makeName = searchParamForVehiculeMakeName(req) 
    model = searchParamForVehiculeModel(req)
    
    if year != None and makeName != None and model != None: 
        makeObject = getMakeId_ByName(makeName) 
        modelObject = getModelId_byYearMake(year,makeObject.get('makeId'), model)
        
        submodelListObject = getSubModelListObject_byYearMakeModel(year,makeObject.get('makeId'), modelObject.get('modelId'))
        submodelObject = searchParamSubModelIdInText(req, submodelListObject)

        if submodelObject != None:
            print('got a submodelid')
            engineListObject = getEngineListObject_byYearMakeModelSubmodel(year,makeObject.get('makeId'), modelObject.get('modelId'), str(submodelObject.get('submodelId')))
            engineObject = searchParamEngineIdInText(req, engineListObject) 

            if engineObject != None:
                print('got a engineid'+str(engineObject.get('engineId')))
                vehicleObject = getVehicle_byYearMakeModelSubmodelEngine(year, makeObject.get('makeId'), modelObject.get('modelId'), submodelObject.get('submodelId'), engineObject.get('engineId'), engineObject.get('engineName'))
                print('baseVehicleId: '+str(vehicleObject.get('baseVehicleId')))
                
                print('engineVinId'+str(engineObject.get('engineParams').get('engineVinId')))
                print('engineDesignationId'+str(engineObject.get('engineParams').get('engineDesignationId')))
                print('engineVersionId'+str(engineObject.get('engineParams').get('engineVersionId')))
                print('fuelTypeId'+str(engineObject.get('engineParams').get('fuelTypeId')))
                print('cylinderHeadTypeId'+str(engineObject.get('engineParams').get('cylinderHeadTypeId')))
                
                response = getCatalogOfParts(year, makeObject, modelObject, submodelObject, engineObject, "Spark Plug")
                 
                #response = carruselResponse_JsonTemplate()
                    
                #print('se genero carrusel: '+makeCard())  
                return json.loads(response)
    response = generateResponseTemplate_Util('In order to help you, We require you to provide at least the following information: name of the piece you need, year, make and model of vehicle.')
    return response 
      
    
def getMakeId_ByName(make): 
    makes = getMakes_APIModel()
    for makep in makes:
        if makep.get('makeName').lower() == make.lower():  
            return makep
        
def getModelId_byYearMake(year, make, model): 
    models = getModels_APIModel(year, make)
    for modelp in models:
        if modelp.get('modelName').lower() == model.lower(): 
            return modelp
        
def getSubModelListObject_byYearMakeModel(year, make, model): 
    submodel = getSubModels_APIModel(year, make, model) 
    return submodel
      
def getEngineListObject_byYearMakeModelSubmodel(year, make, model, submodel):
    engineObject = getEngines_APIModel(year, make, model, submodel) 
    return engineObject  

def getVehicle_byYearMakeModelSubmodelEngine(year, make, model, submodel, engineId, engineName):
    vehicles = getListVehicles_APIModel(year, make, model, submodel, engineId)
    vehicle = insubstring2(engineName, vehicles) 
    return vehicle  

def getCatalogOfParts(year, make, model, submodel, engine, keyword):
    print('juguemos')
    parts = getCatalogOfParts_APIModel(year, make, model, submodel, engine, keyword) 
    if parts != None: 
        print('si tiene algo')
        partes = parts.get('parts') 
        card = ''
        for parte in partes:
            card = card +makeCard(parte)+','
        card = card[:-1]
        return generateContainerCards(str(card)) 
    print('no tiene nada')  
    

     
    
def makeCard(parte):
    img = ''
    for image in parte.get('images'):
        img=img+image.get('preview')
        break
         
    card = '{'
    card = card+'"card": {'
    card = card+'"title": "'+parte.get('partName')+'",'
    card = card+'"imageUri": "'+img+'",'       
    card = card+'"subtitle": "'+parte.get('partId')+'",' 
    card = card+'"buttons": ['
    card = card+'{'
    card = card+'"text": "See Image in full screen",'
    card = card+'"postback": "https://img1.partstech.com/d1/images/f5/ae/1a/w110_f5ae1a42d9980909b920d1b299135a2840a1576d.png"' 
    card = card+'},' 
    card = card+'{'  
    card = card+'"text": "See on web site",'
    card = card+'"postback": "'+parte.get('partsTechCatalogURL')+'"'
    card = card+'}'   
    card = card+']'
    card = card+'}'  
    card = card+'}'  
    return card 

def generateContainerCards(card):
    container = '{'
    container = container+'"fulfillmentText": " hjhvh",' 
    container = container+'"fulfillmentMessages": ['
    container = container+card
    container = container+']'
    container = container+'}'
    #print(str(container)) 
    return str(container)

def addToCart_Service(req, contexto):
    cantidad = getParamFromDlgFlwRequest_Util(req).get('number')
    partid = extractParamVal_Util(req, '#')
    idSender = getSenderIdFromDlgFlwRequest_Util(req)  
    partid = partid.replace("#", "")
                         
    
    usuario = findUser_Model(idSender) 
    if usuario == None: 
        cartObject = createCart_APIModel(partid, cantidad)
        usuario = insertUser_Model(idSender, None, str(cartObject.get('sessionId')))
        
    if usuario.getSessionId() == None:  
        print('usuario NO tiene carrito, es nuevo')
        cartObject = createCart_APIModel(partid, cantidad)
        updateUser_Model(str(cartObject.get('sessionId')), None, idSender)
        usuario = findUser_Model(idSender)
        
    else:
        print('usuario ya tiene carrito')
        addPartToCart_APIModel(partid, cantidad, 2, str(usuario.getSessionId()))
          
    response = generateResponseTemplate_Util('The product has been added correctly.')
    return response 
                                  
     
    
        
    response = generateResponseTemplate_Util('Estas en carrito de compras'+str(cantidad)+' '+str(partid).upper())
    return response
 
def getCart_Service(req, contexto):
    idSender = getSenderIdFromDlgFlwRequest_Util(req)  
    usuario = findUser_Model(idSender) 
    if usuario.getSessionId() == None:
        response = generateResponseTemplate_Util('You dont have products in the cart.')
        return response 
    else:
        print('Sesion: '+str(usuario.getSessionId()))
        cart = getCart_APIModel(usuario.getSessionId())
        orders = cart.get('orders')
        response = ''
        for order in orders:
            response = response+'Store: '+str(order.get('store').get('name'))+'\n'
            response = response+'Total price: '+str(order.get('totalPrice'))+'\n'
            response = response+'Tax: '+str(order.get('tax'))+'\n'
            
            parts = order.get('parts')          
            for part in parts:
                response = response+'PartId: '+str(part.get('partId'))+'\n'
                response = response+'quantity: '+str(part.get('quantity'))+'\n'
                response = response+'Price: '+str(part.get('price').get('cost'))+'\n'
                response = response+'Part Name: '+str(part.get('partName'))+'\n'   
        
        response = generateResponseTemplate_Util('Si hay carrito en tienda: '+response)
        return response

    
############## Search params in text #################
    
def searchParamEngineIdInText(req, engineListObject):  
    engineId = insubstring(req, engineListObject) 
    if engineId != None:
        return engineId
    return None 

def searchParamSubModelIdInText(req, listSubModels):
    for submodel in listSubModels:
        print('oya'+str(submodel.get('submodelName')).lower())
        submodelName = extractParamVal_Util(req, str(submodel.get('submodelName')).lower())  
        print('oya2'+str(submodelName)) 
        if submodelName != None:
            return submodel
    return None  

def searchParamForVehiculeMakeName(req):
    vehicles = allowedVehicules()
    for vehicle in vehicles:
        makeNamet = extractParamVal_Util(req, vehicle.get('makeName'))
        if makeNamet != None:
            if str(makeNamet) == str(vehicle.get('makeName').lower()):
                return makeNamet   
    return None

def searchParamForVehiculeModel(req): 
    vehicles = allowedVehicules()
    for vehicle in vehicles:
        modelt = extractParamVal_Util(req, vehicle.get('model'))    
        if modelt != None:
            return modelt  
    return None

def searchParamForYear(req):
    years = allowedYears()
    for year in years:
        yeart = extractParamVal_Util(req, year.get('year')) 
        if yeart != None:
            if str(yeart) == str(year.get('year')):
                return yeart
    return None
        
     
     
     