#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: gibbygarcia
"""
import pymysql
import os

import json
import requests 
from Persistence.VO.UserVO import UserVO_Class
from Persistence.VO.ProductosVO import ProductosVO_Class
from Persistence.VO.VentasVO import VentasVO_Class
from Persistence.VO.ProductoCarritoVO import ProductosCarritoVO_Class
from Persistence.Data.jsonData import loginCredentials_JsonData
import datetime

apiPartstech = "https://api.beta.partstech.com"
     

def getAccessToken_APIModel():
    url=apiPartstech+"/oauth/access"
    payload = loginCredentials_JsonData()
    headers = {'content-type': 'application/json'}
    r = requests.post(url, data=json.dumps(payload), headers=headers)
    return r.json().get("accessToken")

def getPartTypes_APIModel():
    url = apiPartstech+"/taxonomy/part-types"
    headers = {'content-type': 'application/json', 'Authorization':'bearer '+getAccessToken_APIModel()}
    r = requests.get(url, headers=headers)
    print('esto rulessss') 
    print(r.content)
    return r.json()

def getMakes_APIModel():
    url = apiPartstech+"/taxonomy/vehicles/makes"
    headers = {'content-type': 'application/json', 'Authorization':'bearer '+getAccessToken_APIModel()}
    r = requests.get(url, headers=headers)
    return r.json()

def getModels_APIModel(year, make):
    url = apiPartstech+"/taxonomy/vehicles/models"
    payload = {'year': year, 'make': make}
    headers = {'content-type': 'application/json', 'Authorization':'bearer '+getAccessToken_APIModel()}
    r = requests.get(url, params=payload, headers=headers)
    return r.json()

def getSubModels_APIModel(year, make, model):
    url = apiPartstech+"/taxonomy/vehicles/submodels"
    payload = {'year': year, 'make': make, 'model': model}
    headers = {'content-type': 'application/json', 'Authorization':'bearer '+getAccessToken_APIModel()}
    r = requests.get(url, params=payload, headers=headers)
    return r.json()

def getEngines_APIModel(year, make, model, submodel):
    url = apiPartstech+"/taxonomy/vehicles/engines"
    payload = {'year': year, 'make': make, 'model': model, 'submodel': submodel}
    headers = {'content-type': 'application/json', 'Authorization':'bearer '+getAccessToken_APIModel()}
    r = requests.get(url, params=payload, headers=headers)
    return r.json()
    
def getListVehicles_APIModel(year, make, model, submodel, engine):
    url = apiPartstech+"/taxonomy/vehicles"
    payload = {'year': year, 'make': make, 'model': model, 'submodel': submodel, 'engine':engine}
    headers = {'content-type': 'application/json', 'Authorization':'bearer '+getAccessToken_APIModel()}
    r = requests.get(url, params=payload, headers=headers)
    return r.json()

def getCategories_APIModel():
    url = apiPartstech+"/taxonomy/categories"
    headers = {'content-type': 'application/json', 'Authorization':'bearer '+getAccessToken_APIModel()}
    r = requests.get(url, headers=headers)
    return r.json()

    

def getCatalogOfParts_APIModel(year, make, model, submodel, engine, keyword):
    url = apiPartstech+"/catalog/search"
    payload = {
            "searchParams": {
                "vehicleParams": {
                    "yearId": int(year),
                    "makeId": int(make.get('makeId')),
                    "modelId": int(model.get('modelId')),
                    "subModelId": int(submodel.get('submodelId')),
                    "engineId": int(engine.get('engineId')),
                    "engineParams": {
                        "engineVinId":  int(engine.get('engineParams').get('engineVinId')),
                        "engineDesignationId": int(engine.get('engineParams').get('engineDesignationId')),
                        "engineVersionId": int(engine.get('engineParams').get('engineVersionId')),
                        "fuelTypeId": int(engine.get('engineParams').get('fuelTypeId')),
                        "cylinderHeadTypeId": int(engine.get('engineParams').get('cylinderHeadTypeId')),
                        }
                },
                "keyword": keyword 
              }
            }
    headers = {'content-type': 'application/json', 'Authorization':'bearer '+getAccessToken_APIModel()}
    r = requests.post(url, data=json.dumps(payload), headers=headers)
    return r.json()   
     

def createCart_APIModel(partId, quantity):
    url = apiPartstech+"/punchout/cart/create"
    payload = {
      "orders": [
        {
          "storeId": 1,
          "parts": [
            {
              "partId": partId,
              "lineCardId": 123,
              "quantity": int(quantity)
            }
          ],
          "poNumber": "Some PoNumber",
          "notes": "Some Notes"
        }
      ],
      "urls": {
        "callbackUrl": "http://localhost:80/some-callback-url",
        "callbackOrderUrl": "http://localhost:80/some-callback-order-url",
        "returnUrl": "http://localhost:80/some-return-url",
        "returnResultsUrl": "http://localhost:80/some-return-results-url"
      }
    }
    headers = {'content-type': 'application/json', 'Authorization':'bearer '+getAccessToken_APIModel()}
    r = requests.post(url, data=json.dumps(payload), headers=headers)
    return r.json() 


def addPartToCart_APIModel(partId, quantity, vehicleid, sessionid):
    url = apiPartstech+"/punchout/cart/add-part"
    payload = {
      "sessionId": sessionid,
      "requestedPart": {
        "partId": partId,
        "storeId": 1,
        "lineCardId": 123,
        "quantity": quantity,
        "vehicleId": vehicleid
      }
    }
    headers = {'content-type': 'application/json', 'Authorization':'bearer '+getAccessToken_APIModel()}
    r = requests.post(url, data=json.dumps(payload), headers=headers)
    return r.json() 
     
     
def getCart_APIModel(sessionid):
    url = apiPartstech+"/punchout/cart/info"
    payload = {
      "sessionId": sessionid
    } 
    headers = {'content-type': 'application/json', 'Authorization':'bearer '+getAccessToken_APIModel()}
    r = requests.post(url, data=json.dumps(payload), headers=headers) 
    print(r.content)
    return r.json() 
    
    
    
    
    
    
    

