#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 24 11:29:11 2019

@author: gibbygarcia
"""
# Parent class
class UserVO_Class:

    # Class attribute
    id_usuarios = None
    name = None
    phone = None
    email = None
    address = None
    idfacebook = None
    sessionId = None

    # Initializer / Instance attributes
    #def __init__():
    #def __init__(self, id, apellidop, apellidom, nombre, telefono, correo, direccion, idfacebook):
      #  self.id = None
      #  self.apellidop = None
      #  self.apellidom = None
      #  self.nombre = None
      #  self.telefono = None
      #  self.correo = None
      #  self.direccion = None
      #  self.idfacebook = None
        
    def __init__(self, idusu, name, phone, email, address, idfacebook, sessionId):
        self.id_usuarios = idusu
        self.name = name
        self.phone = phone
        self.email = email
        self.address = address
        self.idfacebook = idfacebook 
        self.sessionId = sessionId

    # instance method
    #def setId(idusu):
     #   id=idusu
        
    def getId(self): 
        return self.id_usuarios 
    
    def getUserName(self): 
        return self.name 
    
    def getSessionId(self):
        return self.sessionId
        
    # instance method
    #def description(self):
    #    return "{} is {} years old".format(self.name, self.age)

    # instance method
    #def speak(self, sound):
    #    return "{} says {}".format(self.name, sound)