#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 24 15:57:34 2019

@author: gibbygarcia
"""

# Parent class
class ProductosVO_Class:

    # Class attribute
    idp = None
    nombre = None
    modelo = None
    marca = None
    color = None
    talla = None
    precio = None
    stock = None
        
    def __init__(self, idp, nombre, modelo, marca, color, talla, precio, stock):
        self.idp = idp
        self.nombre = nombre
        self.modelo = modelo
        self.marca = marca
        self.color = color
        self.talla = talla
        self.precio = precio
        self.stock = stock

    def getId(self): 
        return self.idp
    
    def setId(self, idp): 
        self.idp = idp
    
    def getPrecio(self): 
        return self.precio
    
    def setPrecio(self, precio):
        self.precio=precio
        
    def getCantidad(self): 
        return self.stock
    
    def setCantidad(self, stock): 
        self.stock = stock
        
    
    