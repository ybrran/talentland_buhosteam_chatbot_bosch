#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 24 21:45:06 2019

@author: gibbygarcia
"""

class VentasVO_Class:
    
    idventas=None
    total=None
    fecha=None
    usuario_idfacebook=None
    estatus_id=None
    pago_id=None
    fh_creacion=None
    fh_modificacion = None
    
    
    def setVentasId(self, idventas):
        self.idventas=idventas
    
    def getVentasId(self):
        return self.idventas
    
    def setTotal(self, total):
        self.total=total
    
    def getTotal(self):
        return self.total
    
    def setFecha(self, fecha):
        self.fecha=fecha
    
    def getFecha(self):
        return self.fecha
    
    def setIdUsuario(self, usuario_idfacebook):
        self.usuario_idfacebook=usuario_idfacebook
    
    def getIdUsuario(self):
        return self.usuario_idfacebook
    
    def setEstatusId(self, estatus_id):
        self.estatus_id=estatus_id
    
    def getEstatusId(self):
        return self.estatus_id
    
    def setPagoId(self, pago_id):
        self.pago_id=pago_id
    
    def getPagoId(self):
        return self.pago_id
    
    def setFechaCreacion(self, fh_creacion):
        self.fh_creacion=fh_creacion
    
    def getFechaCreacion(self):
        return self.fh_creacion
    
    def setFechaModificacion(self, fh_modificacion):
        self.fh_modificacion=fh_modificacion
    
    def getFechaModificacion(self):
        return self.fh_modificacion
    