#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 23 17:04:01 2019

@author: gibbygarcia
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 20 08:27:39 2019

@author: gibbygarcia
"""

def intentsResponses_JsonData():
    responses= [{
		"id": 1,
		"intent": "Thanks",
         "responses":[
             {'fulfillmentText': 'Your welcome'},
             {'fulfillmentText': 'is my pleasure!'},
             {'fulfillmentText': 'I am committed to serve you!'},
             {'fulfillmentText': 'I am here to help you!'}
             
         ]   
	},{
		"id": 2,
		"intent": "Name",
        "responses":[
             {'fulfillmentText': 'My name is Chatbot. I am here to help you to consult auto parts information, check price and availability of articles, order and purchase in our store.'}      
         ] 
	}
  
]
    return responses

def loginCredentials_JsonData():
    credentials= {
  "accessType": "user",
  "credentials": {
    "user": {
      "id": "hackteam_17",
      "key": "70e8c0e71d3445bd8eed7b453641a4e4"
    },
    "partner": {
      "id": "beta_bosch",
      "key": "4700fc1c26dd4e54ab26a0bc1c9dd40d"
    }
  }
}
    return credentials

def allowedVehicules():
    vehicles=[
             {'makeName':'BMW',
              'model':'325'
             },
             {'makeName':'BMW',
              'model':'328'
             },
              {'makeName':'Volkswagen',
              'model':'Passat'
             },
             {'makeName':'Volkswagen',
              'model':'Jetta'
             }
         ]
    return vehicles

def allowedYears():
    years=[
             {'year': '2010'},
             {'year': '2011'},
             {'year': '2012'},
             {'year': '2013'},
             {'year': '2014'},
             {'year': '2015'}          
         ]
    return years
    
    
    
    
    
    
    
 