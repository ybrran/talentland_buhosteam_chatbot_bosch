#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: gibbygarcia
"""
import os

from flask import Flask, jsonify, request
from Services.chatBotService import dispatcher_Service, addToCart_Service
from Persistence.Data.jsonData import loginCredentials_JsonData
from Persistence.Data.templatesData import cardResponse_JsonTemplate
from Utils.chatBotUtils import *
from Persistence.VO.UserVO import UserVO_Class
from Persistence.VO.VentasVO import VentasVO_Class

app = Flask(__name__)
contexto="https://572f859d.ngrok.io" 
 
@app.route("/", methods=["POST"])
def chatbot(): 
    req = request.get_json(silent=True, force=True)
    print('ojo') 
    print(req)  
      
    project_id = os.getenv('DIALOGFLOW_PROJECT_ID') 
    app_credentials = os.getenv('GOOGLE_APPLICATION_CREDENTIALS')   
     
    response = dispatcher_Service(req, contexto)
    return jsonify(response)   
 

@app.route("/addToCart/<partId>/<idfacebook>", methods=["GET"])
def addToCart(partId,idfacebook):
    response = addToCart_Service()  
    return jsonify(response)    
    
if __name__ == '__main__': 
    port = int(os.getenv('PORT', 5000))  
    app.run(debug=True, port=port, host='127.0.0.1')
    