#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: gibbygarcia
"""
#import pytesseract
#from PIL import Image
#from PIL import ImageFilter
from io import StringIO
import numpy as np
import os
from urllib.request import urlopen
try:
    from PIL import Image
except ImportError:
    import Image
import pytesseract

#pytesseract.pytesseract.tesseract_cmd = r'/Users/gibbygarcia/anaconda3/pkgs/tesseract-3.05.02-h1ccaaf6_0'
constStorage='/Users/gibbygarcia/GITLAB2/ChatBotUnitis/unitisocrfacebookchatbot/Server/Processing/'

def proccessOCRImage_OcrProcessing(imgurl):
    url = imgurl
    img2 = Image.open(urlopen(url)) 
    
    fileName='imgtemporal.jpg'
    img2.save(constStorage+fileName)
    
    img = Image.open(constStorage+fileName)
    text = "Esta es la información de tu ticket de compra: \n \n "
    text = text+pytesseract.image_to_string(img, lang="spa")
    text = text+" \n \nLamentablemente esta información no se encuentra en nuestra base de datos. Puedo ayudarte con algo más?"
    #print(str(text))
    
    if os.path.exists(constStorage+fileName):
        os.remove(constStorage+fileName)
    else:
        print("The file does not exist")
      
    return str(text)