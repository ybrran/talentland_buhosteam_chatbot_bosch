#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: gibbygarcia
"""
import random
import json

def getIntentName_FromDlgFlwRequest_Util(req):
    return req.get('queryResult').get('intent').get("displayName") 

def getImageAttached_Util(req):
    return req.get('originalDetectIntentRequest').get('payload').get('data').get('message').get('attachments')[0].get('payload').get('url')

def getTxtMessageFromDlgFlwRequest_Util(req):
    return req.get('queryResult').get('queryText')

def getParamFromDlgFlwRequest_Util(req):
    return req.get('queryResult').get('parameters')

def getSenderIdFromDlgFlwRequest_Util(req):
    return req.get('queryResult').get('outputContexts')[0].get('parameters').get('facebook_sender_id')

def getResponseToSendFromJSON_Util(jsonObject, intent): 
    for obj in jsonObject:
      if obj.get("intent") == intent:
          return random.choice(obj.get("responses"))
      
def insubstring(req, listObject):
    text = getTxtMessageFromDlgFlwRequest_Util(req)
    print(text)
    for objecto in listObject:
        print(str(objecto.get('engineName')).lower()) 
        if str(objecto.get('engineName')).lower() in text.lower(): 
            return objecto
        
def insubstring2(text, listObject):
    text = text.lower()
    print('esta entrando: '+text)
    for objecto in listObject:
        print(str(objecto.get('vehicleName')).lower()) 
        if text.lower() in str(objecto.get('vehicleName')).lower(): 
            print('si es igual')
            return objecto
    print('No es igual') 
    return None
 
def extractParamVal_Util(req, pstartsWith):
    text = getTxtMessageFromDlgFlwRequest_Util(req)
    text = text.lower()
    #print('text: '+pstartsWith.lower())  
    for word in text.split():
        if word.startswith(pstartsWith.lower()):
            return word 
        
def generateResponseTemplate_Util(response):
    return {'fulfillmentText': response}

def replaceContextoIntoCard_Util(req,contexto, idTicket):
    result = json.dumps(req).replace('{0}', contexto)
    result = result.replace('{1}', str(idTicket))
    response = json.loads(result) 
    return response

def isNumber_Util(value):
    try:
        val = int(value) 
        return True
    except ValueError:
       return False
